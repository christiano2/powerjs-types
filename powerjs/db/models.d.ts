import base from "powerjs/db/models/base";
import Clientes from "powerjs/db/models/Clientes";
import MunicipiosIbge from "powerjs/db/models/MunicipiosIbge";
import ClientesContatos from "powerjs/db/models/ClientesContatos";
import ClientesHasInternetplanos from "powerjs/db/models/ClientesHasInternetplanos";
import Internetplanos from "powerjs/db/models/Internetplanos";
import DescontosAcrescimos from "powerjs/db/models/DescontosAcrescimos";
import Cobrancas from "powerjs/db/models/Cobrancas";
import Bancos from "powerjs/db/models/Bancos";
import CobrancasBoleto from "powerjs/db/models/CobrancasBoleto";
import CobrancasParcelas from "powerjs/db/models/CobrancasParcelas";
import RadiusGroupreply from "powerjs/db/models/RadiusGroupreply";
import RadiusHotspotGroupreply from "powerjs/db/models/RadiusHotspotGroupreply";

export module models {
    export function get(modelName: string, modelReations: object);

    export function addConnection(classOrNamespace: string, db: object);

    export { base };
}
export default models;


export {
    base,
    Bancos,
    Clientes,
    ClientesContatos,
    ClientesHasInternetplanos,
    Cobrancas,
    CobrancasBoleto,
    CobrancasParcelas,
    DescontosAcrescimos,
    Internetplanos,
    MunicipiosIbge,
    RadiusGroupreply,
    RadiusHotspotGroupreply
};

