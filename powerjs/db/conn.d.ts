export default class conn {
    /**
     * @param dsn
     * @param charset default "utf-8"
     * @param fetchMode default 3 - ADODB_FETCH_BOTH
     */
    static newADOConnection(dsn: string, charset?: string, fetchMode?: number): object | false;

    static createNewConnection(): void;

    static closeConnection(): void;

    static getConnectionId(): any | false;

    static all(sql: string, arrBind?: object|boolean): object;

    static multiQueryAll(sql: string, arrBind?: object|boolean): object | false;

    static first(sql: string, arrBind?: object|boolean): object | false;

    static multiQueryOne(sql: string, arrBind?: object|boolean): object | false;

    static exec(sql: string, arrBind?: object|boolean): object | false;

    static lastId(): number | false;

    static beginTransaction(): boolean;

    static commitTransaction(): boolean;

    static rollback(): void;

    static getErrorMsg(): string;

    static getErrorNo(): number;

    static getDb(): object;

    static setDb(db: object): void;

    /**
     * desabilita a checagem de FK
     * SET FOREIGN_KEY_CHECKS=0
     */
    static disableFk(): void;

    /**
     * habilita a checagem de FK
     * SET FOREIGN_KEY_CHECKS=1
     */
    static enableFk(): void;

    static getErrorDb(): boolean|string;

    /**
     * strings de escape do MySQL
     *
     * @param unescaped
     */
    static escapeString(unescaped: string): string;
}