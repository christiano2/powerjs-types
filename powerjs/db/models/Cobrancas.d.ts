import {base} from "powerjs/db/models";
import Bancos from "powerjs/db/models/Bancos";
import CobrancasBoleto from "powerjs/db/models/CobrancasBoleto";
import CobrancasParcelas from "powerjs/db/models/CobrancasParcelas";

export default class Cobrancas extends base {
    Bancos: Bancos;
    CobrancasBoleto: CobrancasBoleto;
    CobrancasParcelas: CobrancasParcelas[];

    static readonly ORIGEM_INTERNET = 'INTERNET';
    static readonly ORIGEM_ORCAMENTO = 'ORCAMENTO';
    static readonly ORIGEM_COMPRA = 'COMPRA';
    static readonly ORIGEM_VENDA = 'VENDA';
    static readonly ORIGEM_ORDEM_COMPRA = 'ORDEM_COMPRA';
    static readonly ORIGEM_OUTRO_SERVICO = 'OUTRO_SERVICO';
    static readonly ORIGEM_OUTRO = 'OUTRO';
    static readonly ORIGEM_OS = 'ORDEM_SERVICO';
    static readonly ORIGEM_TELEFONIA = 'TELEFONE';

    static readonly METODO_BOLETO = 'BOLETO';
    static readonly METODO_CREDITO = 'CREDITO';
    static readonly METODO_DEBITO = 'DEBITO_AUTOMATICO';
    static readonly METODO_PIX = 'PIX';

    static readonly STATUS_ABERTO = 0;
    static readonly STATUS_PAGO = 1;
    static readonly STATUS_CANCELADO = 2;

    static readonly OCORRENCIA_NOVA = 1;
    static readonly OCORRENCIA_CANCELAMENTO = 2;
    static readonly OCORRENCIA_VENC_ALTERADO = 6;

    static readonly TIPO_MENSALIDADE = 1;
    static readonly TIPO_MANUTENCAO = 2;
    static readonly TIPO_DIVERSOS = 3;
    static readonly TIPO_INSTALACAO = 4;
    static readonly TIPO_ORCAMENTO = 5;
    static readonly TIPO_VENDA = 6;
    static readonly TIPO_RECISAO_INTERNET = 7;
    static readonly TIPO_OS = 8;

    setImportacao(importacao: true | boolean);
    setSetSequencial(setSequencial: false | boolean);
    setGeraNossoNumero(gerarNossoNumero: false | boolean);
}