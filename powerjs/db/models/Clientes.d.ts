import {base, ClientesContatos, ClientesHasInternetplanos} from "powerjs/db/models";

export default class Clientes extends base {
    static TIPO_PESSOA_FISICA: 'F';
    static TIPO_PESSOA_JURIDICA: 'J';
    ClientesHasInternetplanos: ClientesHasInternetplanos[];
    ClientesContatos: ClientesContatos[];
}