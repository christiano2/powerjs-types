
declare class base {
    static namespacePrefix: string;
    static namespace: string;
    static relations: Record<string, any>;
    protected static classConstructor: any;

    constructor();

    Insert();
    Save();
    Update();

    static namespaceClass(className: string): string;

    static setupModel(): void;

    static setRelations(modelName: string, relations: object): void;

    static addConnection(classOrNamespace: string, connection: object): void;

    /**
     * @param className
     * @param returnDefault default true
     */
    static getConnection(className: string, returnDefault?: true|boolean): object;

    static rowCount(where?: string, bindArr?: object): number | false;

    static toList(displayField?: null | string, key?: null | string, whereOrderBy?: '' | string, bindArr?: {} | object | null): object | null;

    static all(where?: string, bindArr?: false | object, extra?: {} | object): any[];

    static first(where?: string, bindArr?: false | object): any;

    static firstId(id: number): any;

    static getLastInsertID(): number | boolean;

    static getNextInsertID(): number;

    /**
     * Cria o model preenchendo os campos com os valores do request
     * @param request
     */
    static create(request: object): object;

    /**
     * Cria o modelo a partir do array
     * @param arr
     */
    static createByArray(arr: object): object;

    /**
     * Cria uma instância do model e preenche o campo ID com o valor informado.
     * Caso o id não seja numérico ou seja nulo, não será preenchido
     * @param id
     */
    static createById(id: number): object;
}

export default base;