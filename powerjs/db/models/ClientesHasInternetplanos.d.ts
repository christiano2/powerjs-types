import {base} from "powerjs/db/models";
import Internetplanos from "powerjs/db/models/Internetplanos";
import DescontosAcrescimos from "powerjs/db/models/DescontosAcrescimos";

export default class ClientesHasInternetplanos extends base {
    Internetplanos: Internetplanos;
    DescontosAcrescimos: DescontosAcrescimos[];

}