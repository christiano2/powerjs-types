declare namespace php {
    export function gc_collect_cycles(): void;
    export function sprintf(...args: any): string;
}