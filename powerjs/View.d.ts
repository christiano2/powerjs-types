export default class View {
    static set(paramName: string, paramValue: any): View;
    static render(viewName: string): View;
}