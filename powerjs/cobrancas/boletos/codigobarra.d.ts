declare class codigobarra {
    /**
     * Extrai os 3 dígitos do banco na linha digitavel
     * e retorna uma instancia da classe
     * de codigo de barras do banco correspondente

     * @param linha
     * @return CodigoBarra|null
     */
    public static parseBancoLinhaDigitavel(linha: string): codigobarra | null;

    /**
     * @return string
     */
    getNossoNumero(): string;

    getDvNossoNumero(): string;

    getSequencial(): string;

    checkDadosFixos(banco: object);

}

export default codigobarra;