export default class ThreadManager {
    /**
     * @param maxChilds default 4
     */
    constructor(maxChilds?: number);

    public getPid(): number;

    public daemonize(): number;

    public static getCpuCount(): number;

    public waitChildsFinish(): void;

    public waitPoolPosition(): number;

    /**
     *
     * @param closure
     * @param args
     */
    public startProcessOnFreePosition(closure: any, ...args): void;

    public getPoolPosition(): number | null;

    /**
     * @param closure
     * @param $args
     */
    public startProcess(closure: any, ...$args): void;

    public getThisThreadNumber(): number;

    public sigHandler(sig: number, info?: any): void
}

