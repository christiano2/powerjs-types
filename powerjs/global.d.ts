declare function var_dump(...expression: any): void;
declare function print(arg: string): void;
declare function exit(): void;
declare function sleep(seconds: number): void;

/**
 * V8 gargabe collector
 */
declare function gc(): void;