export default class Controller {
    get(action: string, callback: any);
    head(action: string, callback: any);
    post(action: string, callback: any);
    put(action: string, callback: any);
    delete(action: string, callback: any);
    connect(action: string, callback: any);
    options(action: string, callback: any);
    patch(action: string, callback: any);
    purge(action: string, callback: any);
    trace(action: string, callback: any);
    addMethodAction(method: string, action: string, callback: any);
    exists(method: string, action: string);
    callAction(method: string, action: string, params: object);
}