export default class data {
    static readonly DATA_HORA_BR: 'd/m/Y H:i';
    static readonly DATA_HORA_US: 'm/d/Y H:i';
    static readonly DATA_HORA_DB: 'Y-m-d H:i:s';
    static readonly DATA_BR: 'd/m/Y';
    static readonly DATA_US: 'm/d/Y';
    static readonly DATA_DB: 'Y-m-d';

    static readonly MESES_POR_EXTENSO: {
        1: 'Janeiro',
        2: 'Fevereiro',
        3: 'Março',
        4: 'Abril',
        5: 'Maio',
        6: 'Junho',
        7: 'Julho',
        8: 'Agosto',
        9: 'Setembro',
        10: 'Outubro',
        11: 'Novembro',
        12: 'Dezembro'
    };

    static convertDataHoraBrToDb(data: string);

    static convertDataHoraDbToBr(data: string);

    static convertDataBrToDb(data: string);

    static convertDataDbToBr(data: string);

    /**
     *
     * @param data
     * @param de padrão DATA_HORA_BR
     * @return \DateTime ou false
     */
    static converToDateTime(data: string, de?: string): object | false;

    /**
     *
     * @param data
     * @param de DATA_HORA_BR
     * @param para DATA_HORA_DB
     */
    static converTo(data: string, de?: string, para?: string): string | false;

    /**
     *
     * @param data
     * @param de DATA_BR
     * @param para DATA_DB
     */
    static converToData(data: string, de?: string, para?: string): string | false;

    /**
     *
     * @param formato DATA_HORA_DB
     */
    static getDataHoraAtual(formato: string): string | false;

    /**
     * Retorna um objeto DateTime com a data atual e hora zerada (utilizado para comparacoes entre datas)
     * @return \DateTime
     */
    static getObjDataAtual(): object;

    static getDataInicioFimSemana();

    static getDataAmanha();

    /**
     * Retorna o próximo dia útil para a data informada
     * @param data \DateTime
     * @return \DateTime
     */
    static getProximoDiaUtil(data: object): object;

    /**
     * Retorna o último dia útil para a data informada
     * @param data \DateTime
     * @return \DateTime
     */
    static getUltimoDiaUtil(data: object): object;

    /**
     * @param data string|\DateTime
     * @return number
     * @throws \Exception
     */
    static getUltimoDiaMes(data: string | object): number;

    /**
     * @param data string|\DateTime
     * @return string
     */
    static convertAnyToDb(data: string | object): string;

    /**
     * @param data string|\DateTime
     * @return \DateTime
     */
    static convertAnyToDateTime(data: string | object): object;

    /**
     * Adiciona dias em uma data
     * @param data
     * @param dias
     * @return \DateTime
     * @throws \Exception
     */
    public static addDiasData(data: string|object, dias: number);

    /**
     * Adiciona meses a uma data
     * @param data
     * @param meses
     * @return \DateTime
     * @throws \Exception
     */
    public static addMesesData(data: string|object, meses: number)

    /**
     * Adiciona um ou mais meses a uma data
     * @param data \DateTime $data
     * @param meses padrão 1
     * @return \DateTime
     * @throws \Exception
     */
    public static addMesData(data: object, meses?: 1|number)

    /**
     * Substrai um ou mais meses a uma data
     * @param data DateTime
     * @param meses padrao 1
     * @return \DateTime
     * @throws \Exception
     */
    public static subMesData(data: object, meses?: 1|number)

    /**
     * Subtrai dias em uma data
     * @param data
     * @param dias
     * @return \DateTime
     * @throws \Exception
     */
    public static subDiasData(data: string | object, dias: number)

    /**
     * Subtrai meses a uma data
     * @param data
     * @param meses
     * @return \DateTime
     * @throws \Exception
     */
    public static subMesesData(data: string | object, meses: number)

    /**
     * Verifica se a string é uma data
     * @param data
     * @return bool
     */
    public static isData(data: string): boolean;

    /**
     * Calcula diferenca de dias entre duas datas
     * @param data1 \DateTime
     * @param data2 \DateTime
     * @return int
     */
    public static diferencaDias(data1: object, data2: object): number

    /**
     * Retorna a diferenca de meses
     * @param data1 \DateTime $data1
     * @param data2 \DateTime $data2
     * @return int
     */
    public static diferencaMeses(data1: object, data2: object): number

    /**
     * Retorna a diferenca de anos
     * @param data1 \DateTime $data1
     * @param data2 \DateTime $data2
     * @return int
     */
    public static diferencaAnos(data1: object, data2: object)

    public static isDataVazio(data?: string)

    public static isDataHoraVazio(data?: string)

    public static getDataHoraDbToHora(datahora: string)

}
