declare enum STR_PAD {
    STR_PAD_LEFT,
    STR_PAD_RIGHT,
    STR_PAD_BOTH
}

export default class numerico {
    /**
     * Transforma a string em um float
     * @param str
     * @param separadorDecimal = ','
     * @param separadorMilhar = '.'
     * @return float
     */
    public static toFloat(str: string, separadorDecimal?: ','|string, separadorMilhar?: '.'|string): number

    /**
     * Arredonda um numero utilizando half even
     * @param valor float|int|mixed
     * @param precisao = 2
     * @return float
     */
    public static round(valor: number|any, precisao: 2|number): number

    public static stringToFloat(str: string): number

    /**
     * Transforma o float em moeda
     * @param float
     * @param currency = false
     * @param separadorDecimal string|Moedas|null = null
     * @param separadorMilhar string|null = null
     * @param casasDecimais int = 2
     * @return string
     */
    public static floatToMoeda(float: number, currency?: false|boolean, separadorDecimal?: null|string|object, separadorMilhar?: null|string, casasDecimais?: 2|number): string

    /**
     * Retorna apenas os numeros
     * @param str
     * @return null|string|string[]
     */
    public static apenasNumero(str: string)

    /**
     * Formata numero com o tamanho e as propriedades especificadas
     * @param numero
     * @param tamanho
     * @param pad = 0
     * @param padDirecao default STR_PAD_LEFT
     * @return bool|string
     */
    public static formataNumero(numero: string|number, tamanho: number, pad?: 0|number, padDirecao?: 0|STR_PAD)

    /**
     * arredonda o valor em intervalos de 5 ou 10 centavos conforme configurado no sistema
     *
     * @param valor
     * @return string
     */
    public static arredondamento(valor: string|number)

    /**
     * Calcula o percentual
     * @param valor
     * @param pct
     * @return float|int
     */
    public static calculaPercentual(valor: number, pct: number)
}