export default class request {
    static json(param: string): any;
    static input(param: string): any;
    static ajax(): boolean;
    static isXmlHttpRequest(): boolean;
    static isMethod(method: string): boolean;
    static isAjaxPost(): boolean;
}