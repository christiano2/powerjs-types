export default class jsonmessage {
    static criarErro(content?: string): jsonmessage;
    static criarSucesso(content?: string|""): jsonmessage;
    static criar(content?: string): jsonmessage;
    show(): void;
    setParam(paramName: string, paramValue: any): jsonmessage;
}