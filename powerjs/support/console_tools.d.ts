export default class console_tools {
    /**
     * @return array [line, column]
     */
    public static getCursorPosition(): object

    public static setCursorPosition(line: number, col?: number): void

    /**
     * @param lines = 1
     */
    public static cursorUpLine(lines?: 1|number): void

    /**
     * @return array [ lines, columns ]
     */
    public static getTermSize(): object

    /**
     * Mostra o processo
     * @param valorAtual valor atual da barra
     * @param valorMaximo valor maximo da barra
     * @param msg texto para aparecer depois das stats
     * @param linha número da linha que a barra deve ser impressa - padrão é a linha atual
     * @param coluna coluna que a parra dever se impressa - padrão é na coluna atual
     */
    public static mostraProgresso(valorAtual: number, valorMaximo: number, msg?: ''|string, linha?: null|number, coluna?: null|number): void
}